import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { configure } from 'mobx';

import App from './components/App'

const stores = {

};


// For easier debugging
window._____APP_STATE_____ = stores;

// don't allow state modifications outside actions
configure({ enforceActions: true });

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById('root')
);