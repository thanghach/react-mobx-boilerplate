import React, { Component } from 'react';
import Menu from './MenuComponent';
import {Link} from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <Link className="navbar-brand" to='/'>Mobx React Boilerplate</Link>
                    {/* <ul className="nav navbar-nav">
							<li className="active">
								<a href="#">Images Gallery</a>
							</li>
							<li>
								<a href="#">Weather</a>
							</li>
						</ul> */}

                    

                </div>

                <div className="collapse navbar-collapse navbar-ex1-collapse">
                    <Menu />
                </div>
            </nav>
        )
    }
}
export default Header;