import React, { Component } from 'react';
import { Link, Route } from 'react-router-dom';
import { observer } from 'mobx-react';


const Menulink = ({ to, label }) => {
    return (
        <Route
            exact={true}
            path={to}
            children={({ match }) => {
                var active = match ? 'active' : '';
                return (
                    <li className={active}>
                        <Link to={to}>{label}</Link>
                    </li>
                );
            }}
        />
    );
}

@observer
class Menu extends Component {
    render() {
        return (
            <ul className="nav navbar-nav">
                <Menulink to="/login" label="Login" />
                <Menulink to="/register" label="Register" />
            </ul>
        );
    }
}

export default Menu;