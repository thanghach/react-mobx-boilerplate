import React, {Component} from 'react';
import Master from '../Layout/MasterComponent';
import TitlePage from '../Layout/TitlePageComponent';

class Login extends Component {
    render() {
        return (
            <Master>
                <TitlePage>Login</TitlePage>
                <form action="" method="POST" role="form">
                    <legend>Login</legend>

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" className="form-control" placeholder="Vui lòng nhập Email" />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control"  placeholder="Vui lòng nhập password" />
                    </div>

                    <button type="submit" className="btn btn-primary">Đăng nhập</button>
                </form>
            </Master>
        )
    }
}
export default Login;