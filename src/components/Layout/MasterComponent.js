import React, { Component, Fragment} from 'react';
import Header from '../Common/HeaderComponent';
import Footer from '../Common/FooterComponent';

class Master extends Component {
    render() {
        return (
            <div className="container">
                
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <Header />
                        {this.props.children}
                        <Footer />
                    </div>
                </div>
                
                
            </div>
        )
    }
}
export default Master;